﻿using UnityEngine;
using System.Collections;

public class ShapeRotationManager : MonoBehaviour {
	//public Vector3 up;

	[SerializeField]
	private GameObject trackingCamera;

	public void Rotate (Vector2 rotation) {
		rotation *= 0.2f;
		transform.Rotate (trackingCamera.transform.up, -rotation.x, Space.World);
		transform.Rotate (trackingCamera.transform.right, rotation.y, Space.World);
	}

//	void Update () {
//		up = trackingCamera.transform.up;
//	}
}
