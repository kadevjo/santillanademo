﻿using UnityEngine;
using System.Collections;

public class ShapeAnimationManager : MonoBehaviour {

	public ShapeType type;

	[SerializeField]
	private Animation shapeAnimation;
	[SerializeField]
	private bool isAssembled = true;
	[SerializeField]
	private bool isAnimating = false;

	private float animationTime;

	void Start () {
	
	}

	public void OnTap () {
		if (this.isAnimating) {
			if (this.isAssembled) {
				this.Animate ("Assembly");
			} else {
				this.Animate ("Disassembly");
			}
		} else {
			if (this.isAssembled) {
				this.Animate ("Disassembly");
			} else {
				this.Animate ("Assembly");
			}

			this.isAnimating = true;
			this.animationTime = Time.time;
		}
	}

	private void Animate (string animation) {
		string animationName = type.ToString () + animation;

		if (this.isAnimating) {
			this.animationTime = Time.time - this.animationTime;
			shapeAnimation [animationName].time = shapeAnimation [animationName].length - this.animationTime;
			shapeAnimation.Play (animationName);
		} else {
			shapeAnimation [animationName].time = 0;
			shapeAnimation.Play (animationName);
		}
	}

//	private void Disassemble () {
//		string animationName = type.ToString () + "Disassembly";
//		
//		if (this.isAnimating) {
//			this.animationTime = Time.time - this.animationTime;
//			shapeAnimation [animationName].time = shapeAnimation [animationName].length - this.animationTime;
//			shapeAnimation.Play (animationName);
//		} else {
//			shapeAnimation [animationName].time = 0;
//			shapeAnimation.Play (animationName);
//		}
//	}

	public void OnDisassemblingDone () {
		this.isAssembled = false;
		this.isAnimating = false;
		this.animationTime = 0;
	}
	
	public void OnAssemblingDone () {
		this.isAssembled = true;
		this.isAnimating = false;
		this.animationTime = 0;
	}

	// Update is called once per frame
	void Update () {
	
	}
}
