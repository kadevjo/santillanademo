﻿using UnityEngine;
using System.Collections;

public class ShapeFaceTouchListener : MonoBehaviour {

	[SerializeField]
	private ShapeTouchListener shapeTouchListener;
	
	public void HandleTap() {
		shapeTouchListener.HandleTap ();
	}

	public void HandleSwipe(Vector2 delta) {
		shapeTouchListener.HandleSwipe (delta);
	}
}
