using UnityEngine;
using System.Collections;

public class TouchManager : MonoBehaviour {

	private bool swiping = false;
	private bool eventSent = false;
	private Vector2 lastPosition;

	private bool isInteractingWithShape = false;
	private GameObject interactionTarget;

	void Update () {
		HandleTouch ();
	}
	
	private void HandleTouch () {
		if (Input.touchCount > 0) {
			//foreach (Touch touch in Input.touches) {
			/*if (Input.GetTouch (0).phase == TouchPhase.Began) {
				Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch (0).position);
				RaycastHit hit;
				if (Physics.Raycast (ray, out hit) && hit.transform.tag == "Shape") {
					interactionTarget = hit.transform.gameObject;
					interactionTarget.transform.GetComponent<ShapeFaceTouchListener>().HandleTap ();
				}
			} else*/ if (Input.GetTouch (0).phase == TouchPhase.Moved) {
				if (swiping == false){
					swiping = true;
					lastPosition = Input.GetTouch(0).position;

					Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch (0).position);
					RaycastHit hit;
					if (Physics.Raycast (ray, out hit) && hit.transform.tag == "Shape") {
						interactionTarget = hit.transform.gameObject;
						isInteractingWithShape = true;
					}
				}
				else{
					if (isInteractingWithShape) {
						Vector2 direction = Input.GetTouch(0).position - lastPosition;
						lastPosition = Input.GetTouch(0).position;				

						interactionTarget.transform.GetComponent<ShapeFaceTouchListener>().HandleSwipe (direction);
					}		
				}
			} else if (Input.GetTouch (0).phase == TouchPhase.Ended) {
					swiping = false;					
					isInteractingWithShape = false;
					interactionTarget = null;
			}
			//}
		}
	}
}
