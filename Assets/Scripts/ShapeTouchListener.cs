using UnityEngine;
using System.Collections;

public enum ShapeType {
	Cube,
	Cone,
	Cylinder
}

public class ShapeTouchListener : MonoBehaviour {
	[SerializeField]
	private ShapeAnimationManager shapeAnimationManager;

	[SerializeField]
	private ShapeRotationManager shapeRotationManager;

	public void HandleTap () {
		shapeAnimationManager.OnTap ();
	}

	public void HandleSwipe (Vector2 deltaPosition) {
		shapeRotationManager.Rotate (deltaPosition);
	}
}
