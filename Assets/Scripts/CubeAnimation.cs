﻿using UnityEngine;
using System.Collections;

public class CubeAnimation : MonoBehaviour {
	private Animation clip;
	public bool trigger = false;

	// Use this for initialization
	void Start () {
		clip = GetComponent<Animation> ();
		clip ["CubeDisassembly"].time = 0.5f;
		clip.Play ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > 1.0f && !trigger) {
			trigger=true;
			clip.Rewind ();
		}
	}
}
